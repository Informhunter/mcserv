package net

import (
	"log"
	"mcserv/proto"
	"net"
)

type AsyncConn interface {
	Close()
	Input() <-chan *proto.Packet
	Output() chan<- *proto.Packet
}

type AsyncListener interface {
	Close()
	Listen(address string) error
	NewConns() <-chan AsyncConn
}

type asyncConn struct {
	conn   net.Conn
	input  chan *proto.Packet
	output chan *proto.Packet
}

type asyncListener struct {
	listener net.Listener
	newConns chan AsyncConn
}

func NewAsyncListener() AsyncListener {
	aListener := new(asyncListener)
	aListener.newConns = make(chan AsyncConn, 10)
	return aListener
}

func (aListener *asyncListener) Listen(address string) (err error) {
	if aListener.listener, err = net.Listen("tcp", address); err != nil {
		return err
	}
	go func() {
		for {
			conn, err := aListener.listener.Accept()
			if err != nil {
				break
			}
			aConn := NewAsyncConn(conn)
			aListener.newConns <- aConn
		}
	}()
	return nil
}

func (aListener *asyncListener) Close() {
	aListener.listener.Close()
}

func (aListener *asyncListener) NewConns() <-chan AsyncConn {
	return aListener.newConns
}

func NewAsyncConn(conn net.Conn) AsyncConn {
	aConn := new(asyncConn)
	aConn.conn = conn
	aConn.input = make(chan *proto.Packet, 10)
	aConn.output = make(chan *proto.Packet, 10)
	go func() {
		for {
			packet, err := proto.ReadPacket(aConn.conn)
			if err != nil {
				close(aConn.input)
				log.Println("Reader goroutine finished.")
				break
			}
			aConn.input <- packet
		}
	}()
	go func() {
		for {
			packet, ok := <-aConn.output
			if !ok {
				log.Println("Writer goroutine finished.")
				break
			}
			err := proto.WritePacket(aConn.conn, packet)
			if err != nil {
				log.Println("Writer goroutine finished.")
				break
			}
		}
	}()
	return aConn
}

func (aConn *asyncConn) Input() <-chan *proto.Packet {
	return aConn.input
}

func (aConn *asyncConn) Output() chan<- *proto.Packet {
	return aConn.output
}

func (aConn *asyncConn) Close() {
	aConn.conn.Close()
	close(aConn.output)
}
