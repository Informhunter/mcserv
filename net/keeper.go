package net

import (
	"errors"
	"mcserv/proto"
)

type ConnKeeper interface {
	RegisterConns() error
	RemoveConn(uuid string) error
	PullPackets() map[string][]*proto.Packet
	PushPacket(uuid string, packet *proto.Packet) error
	//PushPackets(map[string][]*proto.Packet) error
}

func NewConnKeeper(newConnsSource <-chan AsyncConn) ConnKeeper {
	keeper := new(connKeeper)
	keeper.connMap = make(map[string]AsyncConn)
	keeper.newConns = newConnsSource
	return keeper
}

type connKeeper struct {
	newConns <-chan AsyncConn
	connMap  map[string]AsyncConn
}

func (cKeeper *connKeeper) RegisterConns() (err error) {
SELECT_LOOP:
	for {
		select {
		case conn, ok := <-cKeeper.newConns:
			if ok {
				uuid, e := proto.GenUUID()
				if e != nil {
					err = e
					break SELECT_LOOP
				} else {
					cKeeper.connMap[uuid] = conn
				}
			} else {
				err = errors.New("Listener closed")
				break SELECT_LOOP
			}
		default:
			break SELECT_LOOP
		}
	}
	return err
}

func (cKeeper *connKeeper) RegisterConn(uuid string, conn AsyncConn) (err error) {
	if _, exists := cKeeper.connMap[uuid]; exists {
		err = errors.New("Connection with such id already exists")
	} else {
		cKeeper.connMap[uuid] = conn
	}
	return err
}

func (cKeeper *connKeeper) RemoveConn(uuid string) (err error) {
	if _, exists := cKeeper.connMap[uuid]; exists {
		cKeeper.connMap[uuid].Close()
		delete(cKeeper.connMap, uuid)
	} else {
		err = errors.New("No connection with such id")
	}
	return err
}

func (cKeeper *connKeeper) PullPackets() (result map[string][]*proto.Packet) {
	result = make(map[string][]*proto.Packet)
	for key, conn := range cKeeper.connMap {
		input := conn.Input()
	SELECT_LOOP:
		for {
			select {
			case packet, ok := <-input:
				if ok {
					result[key] = append(result[key], packet)
				} else {
					result[key] = append(result[key], nil)
					break SELECT_LOOP
				}
			default:
				break SELECT_LOOP
			}
		}
	}

	return result
}

func (cKeeper *connKeeper) PushPacket(uuid string, packet *proto.Packet) (err error) {
	if conn, exists := cKeeper.connMap[uuid]; exists {
		conn.Output() <- packet
	} else {
		err = errors.New("No connection with such id")
	}
	return err
}
