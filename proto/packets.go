package proto

const HandshakePacketId VarInt = 0x00

type HandshakePacket struct {
	ProtoVersion  VarInt
	ServerAddress String
	ServerPort    UShort
	NextState     VarInt
}

const LoginStartPacketId VarInt = 0x00

type LoginStartPacket struct {
	Name String
}

const LoginSuccessPacketId VarInt = 0x02

type LoginSuccessPacket struct {
	UUID     String
	Username String
}

const KeepAlivePacketId VarInt = 0x00

type KeepAlivePacket struct {
	KeepAliveId VarInt
}

const JoinGamePacketId VarInt = 0x01

type JoinGamePacket struct {
	EntityId         Int
	Gamemode         UByte
	Dimension        Byte
	Difficulty       UByte
	MaxPlayers       UByte
	LevelType        String
	ReducedDebugInfo Boolean
}

const SpawnPositionPacketId VarInt = 0x05

type SpawnPositionPacket struct {
	Location Position
}

const RespawnPacketId VarInt = 0x07

type RespawnPacket struct {
	Dimension  Int
	Difficulty UByte
	Gamemode   UByte
	LevelType  String
}
