package proto

import (
	"bytes"
	"crypto/rand"
	//"encoding/binary"
	"errors"
	"fmt"
	"io"
	"reflect"
)

func ConstructPacket(packetData interface{}) (packet *Packet, err error) {
	packet = new(Packet)
	packet.Id = uint64(PacketTypeToId(reflect.TypeOf(packetData)))
	packet.Data, err = SerializePacketData(packetData)
	if err != nil {
		return nil, err
	} else {
		packet.Length = uint64(len(packet.Data) + BytesInUVarint(packet.Id))
	}
	return packet, nil
}

func SerializePacketData(packet interface{}) ([]byte, error) {
	data := new(bytes.Buffer)
	v := reflect.ValueOf(packet)
	for i := 0; i < v.NumField(); i++ {
		field := v.Field(i).Interface().(WriteTo)
		if _, err := field.WriteTo(data); err != nil {
			return nil, err
		}
	}
	return data.Bytes(), nil
}

func DeserializePacketData(data []byte, packetPointer interface{}) error {
	buffer := bytes.NewBuffer(data)
	v := reflect.ValueOf(packetPointer).Elem()
	for i := 0; i < v.NumField(); i++ {
		field := v.Field(i).Addr().Interface().(ReadFrom)
		if _, err := field.ReadFrom(buffer); err != nil {
			return err
		}
	}
	return nil
}

/*
func SerializePacketData(packet interface{}) ([]byte, error) {
	data := new(bytes.Buffer)
	v := reflect.ValueOf(packet)
	for i := 0; i < v.NumField(); i++ {
		field := v.Field(i)
		switch field.Type().Kind() {
		case reflect.Uint64:
			WriteUVarint(data, field.Uint())
		case reflect.Int64:
			WriteVarint(data, field.Int())
		case reflect.String:
			WriteUVarint(data, uint64(len(field.String())))
			data.Write([]byte(field.String()))
		case reflect.Int16:
			b := make([]byte, 2)
			binary.LittleEndian.PutUint16(b, uint16(field.Int()))
			data.Write(b)
		case reflect.Int32:
			b := make([]byte, 4)
			binary.LittleEndian.PutUint32(b, uint32(field.Int()))
			data.Write(b)
		case reflect.Uint8:
			data.WriteByte(byte(field.Uint()))
		case reflect.Int8:
			data.WriteByte(byte(field.Int()))
		case reflect.Bool:
			var b byte = 0x00
			if field.Bool() {
				b = 0x01
			}
			data.WriteByte(b)
		}
	}
	return data.Bytes(), nil
}

func DeserializePacketData(data []byte, packetPointer interface{}) (err error) {
	buffer := bytes.NewReader(data)
	packet := reflect.ValueOf(packetPointer).Elem()
	packetType := packet.Type()
	for i := 0; i < packetType.NumField(); i++ {
		field := packet.Field(i)
		switch field.Type().Kind() {
		case reflect.Uint64:
			var value uint64
			value, _, err = ReadUVarint(buffer)
			if err != nil {
				return err
			}
			field.Set(reflect.ValueOf(value))

		case reflect.Int64:
			var value int64
			value, _, err = ReadVarint(buffer)
			if err != nil {
				return err
			}
			field.Set(reflect.ValueOf(value))

		case reflect.String:
			var size uint64
			size, _, err = ReadUVarint(buffer)
			if err != nil {
				return err
			}
			strData := make([]byte, size)
			_, err = buffer.Read(strData)
			field.Set(reflect.ValueOf(string(strData)))

		case reflect.Uint16:
			var value1, value2 byte
			value1, err = buffer.ReadByte()
			if err != nil {
				return err
			}
			value2, err = buffer.ReadByte()
			if err != nil {
				return err
			}
			field.Set(reflect.ValueOf(uint16(value1)<<8 + uint16(value2)))

		case reflect.Int16:
			var value1, value2 byte
			value1, err = buffer.ReadByte()
			if err != nil {
				return err
			}
			value2, err = buffer.ReadByte()
			if err != nil {
				return err
			}
			field.Set(reflect.ValueOf(int16(value1)<<8 + int16(value2)))
		}
	}
	return nil
}*/

func BytesInUVarint(value uint64) int {
	i := 1
	for value >= 0x80 {
		value >>= 7
		i++
	}
	return i
}

func BytesInVarint(value int64) int {
	return BytesInUVarint(uint64(value))
}

func WriteVarint(writer io.Writer, value int64) error {
	uValue := uint64(value) << 1
	if value < 0 {
		uValue = ^uValue
	}
	return WriteUVarint(writer, uValue)
}

func WriteUVarint(writer io.Writer, value uint64) error {
	var buffer bytes.Buffer
	i := 0
	for value >= 0x80 {
		buffer.WriteByte(byte(value) | 0x80)
		value >>= 7
		i++
	}
	buffer.WriteByte(byte(value))
	if _, err := buffer.WriteTo(writer); err != nil {
		return err
	}
	return nil
}

func ReadUVarint(reader io.Reader) (value uint64, bytesRead int, err error) {
	var s uint
	b := make([]byte, 1)
	for i := 0; ; i++ {
		if _, err = reader.Read(b); err != nil {
			return value, i + 1, err
		}
		if b[0] < 0x80 {
			if i > 9 || i == 9 && b[0] > 1 {
				return value, i + 1, errors.New("Overflow")
			}
			return value | uint64(b[0])<<s, i + 1, nil
		}
		value |= uint64(b[0]&0x7f) << s
		s += 7
	}
}

func ReadVarint(reader io.Reader) (value int64, bytesRead int, err error) {
	uValue, bytesRead, err := ReadUVarint(reader)
	value = int64(uValue >> 1)
	if uValue&1 != 0 {
		value = ^value
	}
	return value, bytesRead, err
}

func GenUUID() (string, error) {
	uuid := make([]byte, 16)
	n, err := io.ReadFull(rand.Reader, uuid)
	if n != len(uuid) || err != nil {
		return "", err
	}
	uuid[8] = uuid[8]&^0xc0 | 0x80
	uuid[6] = uuid[6]&^0xf0 | 0x40
	return fmt.Sprintf("%x-%x-%x-%x-%x", uuid[0:4], uuid[4:6], uuid[6:8], uuid[8:10], uuid[10:]), nil
}
