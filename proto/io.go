package proto

import (
	"bytes"
	"io"
)

type Packet struct {
	Length uint64
	Id     uint64
	Data   []byte
}

func ReadPacket(reader io.Reader) (packet *Packet, err error) {
	var packetIdSize int
	packet = &Packet{}
	if packet.Length, _, err = ReadUVarint(reader); err != nil {
		return nil, err
	}

	if packet.Id, packetIdSize, err = ReadUVarint(reader); err != nil {
		return nil, err
	}
	packet.Data = make([]byte, packet.Length-uint64(packetIdSize))
	if _, err = io.ReadAtLeast(reader, packet.Data, len(packet.Data)); err != nil {
		return nil, err
	}
	return packet, nil
}

func WritePacket(writer io.Writer, packet *Packet) error {
	buffer := new(bytes.Buffer)

	WriteUVarint(buffer, packet.Length)
	WriteUVarint(buffer, packet.Id)

	if _, err := buffer.Write(packet.Data); err != nil {
		return err
	}

	if _, err := writer.Write(buffer.Bytes()); err != nil {
		return err
	}

	return nil
}
