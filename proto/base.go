package proto

import (
	"bytes"
	"encoding/binary"
	"errors"
	"io"
	"math"
)

type ReadFrom interface {
	ReadFrom(reader io.Reader) (n int, err error)
}

type WriteTo interface {
	WriteTo(writer io.Writer) (n int, err error)
}

type Boolean bool

func (b Boolean) WriteTo(writer io.Writer) (n int, err error) {
	var data []byte
	if b {
		data = []byte{0x01}
	} else {
		data = []byte{0x00}
	}
	return writer.Write(data)
}

func (b *Boolean) ReadFrom(reader io.Reader) (n int, err error) {
	data := make([]byte, 1)
	if n, err = reader.Read(data); err == nil {
		if data[0] == 0x00 {
			*b = Boolean(false)
		} else {
			*b = Boolean(true)
		}
	}
	return n, err
}

type Byte int8

func (b Byte) WriteTo(writer io.Writer) (n int, err error) {
	return writer.Write([]byte{byte(b)})
}

func (b *Byte) ReadFrom(reader io.Reader) (n int, err error) {
	data := make([]byte, 1)
	if n, err = reader.Read(data); err == nil {
		*b = Byte(data[0])
	}
	return n, err
}

type UByte uint8

func (ub UByte) WriteTo(writer io.Writer) (n int, err error) {
	return writer.Write([]byte{byte(ub)})
}

func (ub *UByte) ReadFrom(reader io.Reader) (n int, err error) {
	data := make([]byte, 1)
	if n, err = reader.Read(data); err == nil {
		*ub = UByte(data[0])
	}
	return n, err

}

type Short int16

func (s Short) WriteTo(writer io.Writer) (n int, err error) {
	data := make([]byte, 2)
	binary.BigEndian.PutUint16(data, uint16(s))
	return writer.Write(data)
}

func (s *Short) ReadFrom(reader io.Reader) (n int, err error) {
	data := make([]byte, 2)
	if n, err = reader.Read(data); err == nil {
		*s = Short(binary.BigEndian.Uint16(data))
	}
	return n, err
}

type UShort uint16

func (us UShort) WriteTo(writer io.Writer) (n int, err error) {
	data := make([]byte, 2)
	binary.BigEndian.PutUint16(data, uint16(us))
	return writer.Write(data)
}

func (us *UShort) ReadFrom(reader io.Reader) (n int, err error) {
	data := make([]byte, 2)
	if n, err = reader.Read(data); err == nil {
		*us = UShort(binary.BigEndian.Uint16(data))
	}
	return n, err
}

type Int int32

func (i Int) WriteTo(writer io.Writer) (n int, err error) {
	data := make([]byte, 4)
	binary.BigEndian.PutUint32(data, uint32(i))
	return writer.Write(data)
}

func (i *Int) ReadFrom(reader io.Reader) (n int, err error) {
	data := make([]byte, 4)
	if n, err = reader.Read(data); err == nil {
		*i = Int(binary.BigEndian.Uint32(data))
	}
	return n, err
}

type Long int64

func (l Long) WriteTo(writer io.Writer) (n int, err error) {
	data := make([]byte, 8)
	binary.BigEndian.PutUint64(data, uint64(l))
	return writer.Write(data)
}

func (l *Long) ReadFrom(reader io.Reader) (n int, err error) {
	data := make([]byte, 8)
	if n, err = reader.Read(data); err == nil {
		*l = Long(binary.BigEndian.Uint64(data))
	}
	return n, err
}

type Float float32

func (f Float) WriteTo(writer io.Writer) (n int, err error) {
	data := make([]byte, 4)
	binary.BigEndian.PutUint32(data, math.Float32bits(float32(f)))
	return writer.Write(data)
}

func (f *Float) ReadFrom(reader io.Reader) (n int, err error) {
	data := make([]byte, 4)
	if n, err = reader.Read(data); err == nil {
		*f = Float(math.Float32frombits(binary.BigEndian.Uint32(data)))
	}
	return n, err
}

type Double float64

func (d Double) WriteTo(writer io.Writer) (n int, err error) {
	data := make([]byte, 8)
	binary.BigEndian.PutUint64(data, math.Float64bits(float64(d)))
	return writer.Write(data)
}

func (d *Double) ReadFrom(reader io.Reader) (n int, err error) {
	data := make([]byte, 8)
	if n, err = reader.Read(data); err == nil {
		*d = Double(math.Float64frombits(binary.BigEndian.Uint64(data)))
	}
	return n, err
}

type String string

func (s String) WriteTo(writer io.Writer) (n int, err error) {
	var l VarInt = VarInt(len(s))
	if n1, err1 := l.WriteTo(writer); err == nil {
		n2, err2 := writer.Write([]byte(s))
		if err2 == nil {
			n = n1 + n2
		} else {
			err = err2
		}
	} else {
		err = err1
	}
	return n, err
}

func (s *String) ReadFrom(reader io.Reader) (n int, err error) {
	var l VarInt
	l.ReadFrom(reader)
	data := make([]byte, uint64(l))
	if n, err = reader.Read(data); err == nil {
		*s = String(data)
	}
	return n, err
}

type VarInt uint64

func (vi VarInt) WriteTo(writer io.Writer) (n int, err error) {
	var buffer bytes.Buffer
	value := uint64(vi)
	for value >= 0x80 {
		buffer.WriteByte(byte(value) | 0x80)
		value >>= 7
		n++
	}
	buffer.WriteByte(byte(value))
	return writer.Write(buffer.Bytes())
}

func (vi *VarInt) ReadFrom(reader io.Reader) (n int, err error) {
	var s uint
	var value uint64
	b := make([]byte, 1)
	for {
		if _, err = reader.Read(b); err == nil {
			if b[0] < 0x80 {
				if n > 9 || n == 9 && b[0] > 1 {
					err = errors.New("Overflow")
					break
				} else {
					value = value | uint64(b[0])<<s
					break
				}
			}
			value |= uint64(b[0]&0x7f) << s
			s += 7
			n++
		}
	}
	if err == nil {
		*vi = VarInt(value)
	}
	return n, err
}

type Position struct {
	X int32
	Y int16
	Z int32
}

func (p Position) WriteTo(writer io.Writer) (n int, err error) {
	var data Long
	data |= Long(p.X) << 38
	data |= Long(p.Y) << 26
	data |= Long(p.Z)
	return data.WriteTo(writer)
}

func (p *Position) ReadFrom(reader io.Reader) (n int, err error) {
	var data Long
	data.ReadFrom(reader)
	p.Z = int32(data)
	p.Y = int16(data >> 12 & 0x0FFF)
	p.X = int32(data >> 48)
	return n, err
}
