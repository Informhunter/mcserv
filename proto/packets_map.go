package proto

import (
	"reflect"
)

var packetTypeToId = map[reflect.Type]VarInt{
	reflect.TypeOf((*HandshakePacket)(nil)).Elem():     HandshakePacketId,
	reflect.TypeOf((*LoginStartPacket)(nil)).Elem():    LoginStartPacketId,
	reflect.TypeOf((*LoginSuccessPacket)(nil)).Elem():  LoginSuccessPacketId,
	reflect.TypeOf((*KeepAlivePacket)(nil)).Elem():     KeepAlivePacketId,
	reflect.TypeOf((*JoinGamePacket)(nil)).Elem():      JoinGamePacketId,
	reflect.TypeOf((*SpawnPositionPacket)(nil)).Elem(): SpawnPositionPacketId,
	reflect.TypeOf((*RespawnPacket)(nil)).Elem():       RespawnPacketId,
}

func PacketTypeToId(packetType reflect.Type) VarInt {
	return packetTypeToId[packetType]
}
