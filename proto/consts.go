package proto

const (
	StateHandshaking VarInt = iota
	StateStatus
	StateLogin
	StatePlay
)

const (
	GamemodeSurvival UByte = iota
	GamemodeCreative
	GamemodeAdventure
	GamemodeSpectator
	GamemodeHardcoreFlag = 0x08
)

const (
	DimensionNether Byte = iota - 1
	DimensionOverworld
	DimensionEnd
)

const (
	DifficultyPeaceful UByte = iota
	DifficultyEasy
	DifficultyNormal
	DifficultyHard
)

const (
	LevelTypeDefault String = "default"
)
