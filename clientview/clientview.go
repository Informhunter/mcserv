package clientview

import (
	"mcserv/net"
	"mcserv/netruler"
	"mcserv/proto"
)

type EntityMovementObserver interface {
	EntityMoved(entityId proto.VarInt, postion proto.Positon)
	EntityRotated(rotation proto.Rotation)
}

type EntityAnimationObserver interface {
	EntityAnimated(animation proto.Animation)
}

type ChunkObserver interface {
	ChunkBlockChanged()
}

type ClientView struct {
	client   *netruler.Client
	position proto.Position
}
