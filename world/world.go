package world

import (
	"errors"
	"log"
	"mcserv/net"
	"mcserv/proto"
	"time"
)

type Player struct {
	State proto.VarInt
}

type World struct {
	Keeper  net.ConnKeeper
	Players map[string]*Player
}

func (world *World) Main() {
	go func() {
		tick := time.Tick(time.Duration(15) * time.Second)
		kaPacket := proto.KeepAlivePacket{0x31337}
		packet, _ := proto.ConstructPacket(kaPacket)
		for {
			<-tick
			for key := range world.Players {
				world.Keeper.PushPacket(key, packet)
			}
		}
	}()
	for {
		world.addNewConnections()
		world.handlePackets()
		world.update()
	}
}

func (world *World) addNewConnections() {
	world.Keeper.RegisterConns()
}

func (world *World) handlePackets() {
	packets := world.Keeper.PullPackets()
	for key, packetList := range packets {
		if _, exists := world.Players[key]; !exists {
			world.Players[key] = &Player{proto.StateHandshaking}
		}
	PACKET_LOOP:
		for _, packet := range packetList {
			if packet == nil {
				log.Println("Removing conn", key)
				world.Keeper.RemoveConn(key)
				delete(world.Players, key)
				continue PACKET_LOOP
			} else {
				world.handlePacket(key, packet)
			}
		}
	}
}

func (world *World) update() {

}

func (world *World) handlePacket(uuid string, packet *proto.Packet) (err error) {
	pConn, exists := world.Players[uuid]
	if !exists {
		err = errors.New("World.handlePacket: Wrong uuid passed")
	} else {
		log.Println("Packet: ", packet)
		switch pConn.State {
		case proto.StateHandshaking:
			if packet.Id == 0 {
				hsPacket := proto.HandshakePacket{}
				err := proto.DeserializePacketData(packet.Data, &hsPacket)
				if err != nil {
					return err
				}
				pConn.State = hsPacket.NextState
				log.Println("HandshakePacket: ", hsPacket)
			}
		case proto.StateLogin:
			if packet.Id == 0 {
				lsPacket := proto.LoginStartPacket{}
				err := proto.DeserializePacketData(packet.Data, &lsPacket)
				if err != nil {
					return err
				}
				log.Println("LoginStartPacket: ", lsPacket)
				lSuccessPacket := proto.LoginSuccessPacket{}
				lSuccessPacket.UUID = proto.String(uuid)
				lSuccessPacket.Username = lsPacket.Name
				p, _ := proto.ConstructPacket(lSuccessPacket)
				log.Println("Sent packet: ", p)
				world.Players[uuid].State = proto.StatePlay
				world.Keeper.PushPacket(uuid, p)
				jgPacket := proto.JoinGamePacket{
					0,
					proto.GamemodeCreative,
					proto.DimensionOverworld,
					proto.DifficultyHard,
					16,
					proto.LevelTypeDefault,
					false}
				p, _ = proto.ConstructPacket(jgPacket)
				log.Println("Sent packet: ", p)
				world.Keeper.PushPacket(uuid, p)
			}
		}
	}
	return err
}
