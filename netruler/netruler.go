package netruler

import (
	"container/list"
	"mcserv/net"
	"mcserv/proto"
	//"mcserv/world"
	"time"
)

type Player interface{}

type World interface{}

type NetRuler struct {
	connListener  net.AsyncListener
	clients       *list.List
	keepAliveTick <-chan time.Time
	world         World
}

func (ruler *NetRuler) Main() {
	kaPacketData := proto.KeepAlivePacket{0x31337}
	kaPacket, _ := proto.ConstructPacket(kaPacketData)
	for {
		select {
		case newConn := <-ruler.connListener.NewConns():
			client := NewClient(newConn, nil)
			ruler.clients.PushBack(client)
		case <-ruler.keepAliveTick:
			for clientEl := ruler.clients.Front(); clientEl != nil; clientEl = clientEl.Next() {
				client := clientEl.Value.(*Client)
				if client.State == proto.StatePlay {
					client.SendPacket(kaPacket)
				}
			}
		default:
			for clientEl := ruler.clients.Front(); clientEl != nil; clientEl = clientEl.Next() {
				client := clientEl.Value.(*Client)
				for _, packet := range client.Packets(10) {
					ruler.HandlePacket(client, packet)
				}
			}
		}
	}
}

func (ruler *NetRuler) HandlePacket(client *Client, packet *proto.Packet) {
	switch client.State {
	case proto.StateHandshaking:
		switch proto.VarInt(packet.Id) {
		case proto.HandshakePacketId:
			hsPacketData := proto.HandshakePacket{}
			proto.DeserializePacketData(packet.Data, &hsPacketData)
			client.State = hsPacketData.NextState
		}
	case proto.StateLogin:
		switch proto.VarInt(packet.Id) {
		case proto.LoginStartPacketId:
			lsPacketData := proto.LoginStartPacket{}
			proto.DeserializePacketData(packet.Data, &lsPacketData)
			lSuccessPacketData := proto.LoginSuccessPacket{}
			lSuccessPacketData.UUID = proto.String(client.UUID)
			lSuccessPacketData.Username = lsPacketData.Name
			lSuccessPacket, _ := proto.ConstructPacket(lSuccessPacketData)
			client.conn.Output() <- lSuccessPacket
			client.State = proto.StatePlay
			jgPacketData := proto.JoinGamePacket{
				0,
				proto.GamemodeCreative,
				proto.DimensionOverworld,
				proto.DifficultyHard,
				16,
				proto.LevelTypeDefault,
				false,
			}
			jgPacket, _ := proto.ConstructPacket(jgPacketData)
			client.conn.Output() <- jgPacket
		}
	}
}

func NewNetRuler(connListener net.AsyncListener, world World) (ruler *NetRuler) {
	ruler = new(NetRuler)
	ruler.clients = list.New()
	ruler.connListener = connListener
	ruler.keepAliveTick = time.Tick(time.Second * 16)
	return ruler
}
