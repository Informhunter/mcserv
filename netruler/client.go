package netruler

import (
	"mcserv/net"
	"mcserv/proto"
)

type Client struct {
	conn          net.AsyncConn
	State         proto.VarInt
	KAIgnoreCount int
	Player        Player
	UUID          string
}

func (c Client) Packets(maxCount int) (packets []*proto.Packet) {
LOOP:
	for i := 0; i < maxCount; i++ {
		select {
		case packet := <-c.conn.Input():
			packets = append(packets, packet)
		default:
			break LOOP
		}
	}
	return packets
}

func (c Client) SendPacket(packet *proto.Packet) (err error) {
	c.conn.Output() <- packet
	return err
}

func (c Client) SendPackets(packets []*proto.Packet) (err error) {
	for _, packet := range packets {
		c.conn.Output() <- packet
	}
	return err
}

func NewClient(conn net.AsyncConn, player *Player) (client *Client) {
	client = new(Client)
	client.conn = conn
	client.State = proto.StateHandshaking
	client.KAIgnoreCount = 0
	client.Player = player
	client.UUID, _ = proto.GenUUID()
	return client
}
