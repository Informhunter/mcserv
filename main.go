package main

import (
	"fmt"
	"mcserv/server"
)

func main() {
	fmt.Println("MCServ 1.0")
	server.Start()
}
