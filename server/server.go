package server

import (
	"log"
	"mcserv/net"
	"mcserv/netruler"
	//"mcserv/world"
)

func Start() {
	log.Println("Starting server")

	listener := net.NewAsyncListener()
	if err := listener.Listen("localhost:25777"); err != nil {
		log.Fatal("Failed to start server on port 25777")
	}
	nr := netruler.NewNetRuler(listener, nil)
	nr.Main()
}
